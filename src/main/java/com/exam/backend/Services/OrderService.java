package com.exam.backend.Services;

import com.exam.backend.Entities.Orderclass;
import com.exam.backend.Repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public List<Orderclass> getAllOrder(){
        return orderRepository.findAll();
    }

    public Orderclass getOrderById(int orderId){
        return orderRepository.findById(orderId).orElseThrow();
    }
    public void deleteOrder(Orderclass order){
        orderRepository.delete(order);
    }
}
