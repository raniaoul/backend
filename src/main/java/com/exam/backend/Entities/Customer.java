package com.exam.backend.Entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCustomer;
    private String name;
    private String address;
    @OneToMany(mappedBy = "customerOrder")
    private List<Orderclass> listOrders = new ArrayList<>();

    public Customer() {
    }

    public Customer(int idCustomer, String name, String address, List<Orderclass> listOrders) {
        this.idCustomer = idCustomer;
        this.name = name;
        this.address = address;
        this.listOrders = listOrders;
    }
}
