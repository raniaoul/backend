package com.exam.backend.Entities.Super;

import com.exam.backend.Entities.Orderclass;
import jakarta.persistence.*;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idPayment;
    private float amount;

    @ManyToOne
    private Orderclass orderPayment;



}
