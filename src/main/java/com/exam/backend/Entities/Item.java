package com.exam.backend.Entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idItem;
    private String description;

    public Item(int idItem, String description) {
        this.idItem = idItem;
        this.description = description;
    }

    public Item() {
    }
}
