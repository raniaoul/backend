package com.exam.backend.Entities;

import com.exam.backend.Entities.Super.Payment;
import jakarta.persistence.Entity;

import java.util.Date;

@Entity
public class Checkclass extends Payment {
    private String number;
    private String type;
    private Date  date;

    public Checkclass(String number, String type, Date date) {
        this.number = number;
        this.type = type;
        this.date = date;
    }

    public Checkclass() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
