package com.exam.backend.Entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int OrderDetail;
    private String quantity;
    private String taxStatus;

    public OrderDetails(int orderDetail, String quantity, String taxStatus, List<Orderclass> listOrders, Item item) {
        OrderDetail = orderDetail;
        this.quantity = quantity;
        this.taxStatus = taxStatus;
        this.listOrders = listOrders;
        this.item = item;
    }

    public OrderDetails() {
    }

    @OneToMany(mappedBy = "orderDetails")
    private List<Orderclass> listOrders = new ArrayList<>();

    @ManyToOne
    private Item item;
}
