package com.exam.backend.Entities;

import com.exam.backend.Entities.Super.Payment;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Orderclass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date date;
    @Enumerated(EnumType.STRING)
     private Status status;

    @OneToMany(mappedBy = "orderPayment")
    private List<Payment> listPayments = new ArrayList<>();


    @ManyToOne
    private OrderDetails orderDetails;

    @ManyToOne
    private Customer customerOrder;

    public Orderclass() {
    }

    public Orderclass(int id, Date date, Status status, List<Payment> listPayments, OrderDetails orderDetails, Customer customerOrder) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.listPayments = listPayments;
        this.orderDetails = orderDetails;
        this.customerOrder = customerOrder;
    }
}
