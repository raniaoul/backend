package com.exam.backend.Controllers;

import com.exam.backend.Entities.OrderDetails;
import com.exam.backend.Entities.Orderclass;
import com.exam.backend.Repositories.OrderRepository;
import com.exam.backend.Services.OrderService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    private OrderService orderService;
    private OrderRepository orderRepository;


    public OrderController(OrderService orderService, OrderRepository orderRepository) {
        this.orderService = orderService;
        this.orderRepository = orderRepository;
    }

    @GetMapping("/getAll")
    public List<Orderclass> getOrder(){
        return orderService.getAllOrder();
    }


}
