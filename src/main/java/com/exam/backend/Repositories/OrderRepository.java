package com.exam.backend.Repositories;

import com.exam.backend.Entities.Orderclass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository <Orderclass, Integer> {

}
